//
// Created by willh on 26/10/2021.
//

#ifndef LINALGEBRA_SPMV_H
#define LINALGEBRA_SPMV_H

#include <omp.h>

#include "Matrix.h"
#include "CSR_Matrix.h"

class SpMV {

public:
    static void run_serial(CSR_Matrix &M, std::vector<double> &v, std::vector<double> &result);
    static void run_shared_memory(CSR_Matrix &M, std::vector<double> &v, std::vector<double> &result);
    static void run_distributed(CSR_Matrix &M, std::vector<double> &v, std::vector<double> &result);
};


#endif //LINALGEBRA_SPMV_H
