//
// Created by willh on 26/10/2021.
//

#ifndef LINALGEBRA_LU_DECOMPOSITION_H
#define LINALGEBRA_LU_DECOMPOSITION_H


class LU_Decomposition {
    void decompose(Matrix<double> M, Matrix<double> L, Matrix<double> U);
    void decompose(CSR_Matrix M, CSR_Matrix L, CSR_Matrix U);
};


#endif //LINALGEBRA_LU_DECOMPOSITION_H
