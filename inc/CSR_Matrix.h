//
// Created by willh on 21/09/2021.
//

#ifndef LINALGEBRA_CSR_MATRIX_H
#define LINALGEBRA_CSR_MATRIX_H

#include <vector>
#include <iostream>
#include <cmath>
//#include <bits/stdc++.h> // only for gcc

class CSR_Matrix {
private:

    int nnz;
    size_t n, m;

    int BLOCKSIZE_N, BLOCKSIZE_M;
    std::vector<CSR_Matrix> blockCSR;

    void adjust_IA(std::vector<int> &v, int pos);
public:

    std::vector<double> A;
    std::vector<int> IA;
    std::vector<int> JA;

    CSR_Matrix();

    ~CSR_Matrix();

    // getters and setters
    void set_nnz(int value);
    void set_n(size_t value);
    void set_m(size_t value);

    int get_nnz() const;
    size_t get_n() const;
    size_t get_m() const;

    size_t rows() const;
    size_t columns() const;

    void set_a(std::vector<double> vector);
    void set_ia(std::vector<int> vector);
    void set_ja(std::vector<int> vector);

    // serial mat mul
    void mul(CSR_Matrix &Mat);

    // auxiliar methods
    double get_element(int i, int j);
    void print(std::ostream &out);

    // distributed processing - in development
    void set_blocksize(int blocksize_n, int blocksize_m);
    void decompose();

};


#endif //LINALGEBRA_CSR_MATRIX_H
