//
// Created by willh on 30/08/2021.
//

#ifndef LINALGEBRA_MATRIX_H
#define LINALGEBRA_MATRIX_H


#include <cassert>
#include <cmath>
#include <iomanip>
#include <iostream>
#include <vector>

template <typename scalar_type>
class Matrix
{
public:

    Matrix(size_t rows, size_t columns)
            : rows_(rows), columns_(columns), elements_(rows * columns) {}

    Matrix(size_t rows, size_t columns, scalar_type value)
            : rows_(rows), columns_(columns), elements_(rows * columns, value) {}

    Matrix(size_t rows, size_t columns,
           const std::initializer_list<std::initializer_list<scalar_type>>& values)
            : rows_(rows), columns_(columns), elements_(rows * columns) {
        assert(values.size() <= rows_);
        size_t i = 0;
        for (const auto& row : values) {
            assert(row.size() <= columns_);
            std::copy(begin(row), end(row), &elements_[i]);
            i += columns_;
        }
    }

    Matrix(size_t rows, size_t columns,
           const std::vector<std::vector<scalar_type>>& values)
            : rows_(rows), columns_(columns), elements_(rows * columns) {
        assert(values.size() <= rows_);
        size_t i = 0;

        for (const auto& row : values) {
            assert(row.size() <= columns_);
            std::copy(begin(row), end(row), &elements_[i]);
            i += columns_;
        }
    }

    size_t rows() const { return rows_; }
    size_t columns() const { return columns_; }

    const scalar_type& operator()(size_t row, size_t column) const {
        assert(row < rows_);
        assert(column < columns_);
        return elements_[row * columns_ + column];
    }
    scalar_type& operator()(size_t row, size_t column) {
        assert(row < rows_);
        assert(column < columns_);
        return elements_[row * columns_ + column];
    }

    void print(std::ostream &out){
        out << std::fixed << std::setprecision(5);
        for (size_t row = 0; row < rows_; ++row) {
            for (size_t column = 0; column < columns_; ++column) {
                if (column > 0)
                    out << ' ';
                out << std::setw(9) << elements_[row * columns_ + column];
            }
            out << '\n';
        }
    }

private:
    size_t rows_;
    size_t columns_;
    std::vector<scalar_type> elements_;
};

#endif //LINALGEBRA_MATRIX_H
