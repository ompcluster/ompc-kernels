//
// Created by willh on 30/08/2021.
//

#ifndef LINALGEBRA_CHOLESKY_H
#define LINALGEBRA_CHOLESKY_H

#include "Matrix.h"

class Cholesky {
public:
    
    Matrix<double> cholesky_factor(const Matrix<double>& input);
    
    void print(std::ostream &out, const Matrix <double> &a);
};


#endif //LINALGEBRA_CHOLESKY_H
