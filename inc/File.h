//
// Created by willh on 29/08/2021.
//

#ifndef LINALGEBRA_FILE_H
#define LINALGEBRA_FILE_H

#include <cstdlib>
#include <cstdio>
#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
#include <cerrno>

#include "Matrix.h"
#include "CSR_Matrix.h"


using namespace std;

class File {
private:
    CSR_Matrix read_textfile();

    CSR_Matrix read_binfile();

    std::vector<unsigned char> get_file_contents();

public:
    fstream file;

    File(string filename,  ios::openmode mode);

    ~File();

    CSR_Matrix readCSRMatrix(int file_type = 0);
//    size_t writeFile(void *pointer, size_t size, size_t n);

};


#endif //LINALGEBRA_FILE_H
