#!/usr/bin/env bash

cd cmake-build-debug-wsl || exit

file="sparse20000_0.1p.bin"

mkdir -p ../out

for num_threads in 1 2 4
do
	for run in `seq 1 3`
	do
		export OMP_NUM_THREADS=$num_threads
	
		echo "******************************"
		echo ""
		echo "   run $run running $num_threads threads"
		echo ""
		./linalgebra $file > ../out/spmv_${run}_${num_threads}.out
	done
done
