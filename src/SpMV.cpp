//
// Created by willh on 26/10/2021.
//
#include "../inc/SpMV.h"
void SpMV::run_serial(CSR_Matrix &M, std::vector<double> &v, std::vector<double> &result) {
    unsigned n = M.rows();
    for (int i = 0; i < n; i++) {
        double res = 0.0;
        unsigned int begin = M.IA[i];
        unsigned int end = M.IA[i+1];
        for (unsigned int j = begin; j < end; j++) {
            unsigned int pos = M.JA[j];
            res += M.A[j] * v[pos];
        }
        result[i] = res;
    }
}

void SpMV::run_shared_memory(CSR_Matrix &M, std::vector<double> &v, std::vector<double> &result) {
    unsigned n = M.rows();
#pragma omp parallel for default(none) shared(M, v, result, n)
    for (int i = 0; i < n; i++) {
        double res = 0.0;
        unsigned int begin = M.IA[i];
        unsigned int end = M.IA[i+1];
        for (unsigned int j = begin; j < end; j++) {
            unsigned int pos = M.JA[j];
            res += M.A[j] * v[pos];
        }
        result[i] = res;
    }
}

void SpMV::run_distributed(CSR_Matrix &M, std::vector<double> &v, std::vector<double> &result) {
    int n = (int) M.get_n();
    int block_size = 1000000;
    int num_blocks = ceil((double) n / block_size);

//    printf("num processes: %d\n", omp_get_num_devices());
//    MPI_Comm_size(MPI_COMM_WORLD, &num_blocks);

    const double *data = &M.A[0];
    const int *row_ptr = &M.IA[0];
    const int *indices = &M.JA[0];

    double *v_array = &v[0];
    double *w = (double *)malloc(sizeof(double) * v.size());
//    int block_size = std::max((int)std::ceil((float)n/(float)num_blocks), 1);

    auto *b_tam = new unsigned int[num_blocks];
    auto *b_size = new unsigned int[num_blocks];

#pragma omp parallel
#pragma omp single
    {
        for (unsigned int i=0, block_id=0; i < n; i+=block_size, block_id++) {
            unsigned int line_begin = row_ptr[i];
            b_size[block_id] = std::min((int) (n-i), block_size);
            b_tam[block_id] = row_ptr[i+b_size[block_id]] - line_begin;

#pragma omp target enter data map(to: \
					data[line_begin:b_tam[block_id]], \
					indices[line_begin:b_tam[block_id]]) \
					depend(out: data[line_begin], indices[line_begin]) nowait
        }

        // Send the whole p and row_ptr: could be splitted in block?
#pragma omp target enter data map(to: v_array[:n], row_ptr[:n+1]) depend(out: *v_array, *row_ptr) nowait

        for (unsigned int i=0, block_id=0; i < n; i+=block_size, block_id++) {
            unsigned int line_begin = row_ptr[i];
            unsigned int size = b_size[block_id];
            unsigned int tam = b_tam[block_id];
            
#pragma omp target firstprivate(i, block_id, size) \
							   map(from: w[i:size]) \
							   map(data[line_begin:tam], indices[line_begin:tam]) \
							   depend(in: *v_array, *row_ptr, data[line_begin], indices[line_begin]) \
							   depend(out: w[i]) nowait
            {
#pragma omp parallel for
                for (unsigned int k = i; k < i+size; k++)
                {
                    double res = 0.0;
                    unsigned int begin = row_ptr[i];
                    unsigned int end = row_ptr[i+1];

                    for (unsigned int j = begin; j < end; j++) {
                        unsigned int pos = indices[j];
                        res += data[j] * v_array[pos];
                    }
                    w[i] = res;
                }
            }
        }
//#pragma omp target update to(v_array[:n]) depend(inout: *v_array) nowait
    }


//    for (unsigned int i=0, bid=0; i<n; i+=block_size, bid++)
//    {
//        unsigned int line_begin = row_ptr[i];
//#pragma omp target exit data map(delete: \
//					data[line_begin:b_tam[bid]], \
//					indices[line_begin:b_tam[bid]])
//    }

#pragma omp target exit data map(delete: v_array[:n], row_ptr[:n+1])

    delete[] b_tam;
    delete[] b_size;

    for (int i = 0; i < v.size(); i++)
        result.push_back(w[i]);

}