//
// Created by willh on 21/09/2021.
//

#include "../inc/CSR_Matrix.h"

CSR_Matrix::CSR_Matrix() {
    this->nnz = 0;
    this->n = 0;
    this->m = 0;
}

void CSR_Matrix::print(std::ostream &out) {
    for (int i = 0; i < this->n; i++) {
        for (int j = 0; j < this->m; j++)
            out << CSR_Matrix::get_element(i, j) << " ";
        out << std::endl;
    }

}

double CSR_Matrix::get_element(int i, int j) {

    int p_ini = this->IA[i];
    int p_end = this->IA[i+1];
    double res = 0;

    for (int column_index = p_ini; column_index < p_end; column_index++) {
        if (this->JA[column_index] == j) {
            res = this->A[column_index];
            break;
        }
    }

    return res;
}

void CSR_Matrix::set_a(std::vector<double> vector) {
    // TODO assert nnz
    this->A.clear();
    this->A = std::move(vector);
//    nnz = int(A.size());
}

void CSR_Matrix::set_ia(std::vector<int> vector) {
    // TODO assert n
    this->IA.clear();
    this->IA = std::move(vector);
//    n = IA.size() - 1;
}

void CSR_Matrix::set_ja(std::vector<int> vector) {
    // TODO assert m
    this->JA.clear();
    this->JA = std::move(vector);
//    m = *max_element(std::begin(JA), std::end(JA)) + 1;
}

void CSR_Matrix::set_nnz(int value)
{
    this->nnz = value;
}
void CSR_Matrix::set_n(size_t value)
{
    this->n = value;
}

void CSR_Matrix::set_m(size_t value)
{
    this->m = value;
}

int CSR_Matrix::get_nnz() const {
    return this->nnz;
}

size_t CSR_Matrix::get_n() const {
    return this->n;
}

size_t CSR_Matrix::get_m() const {
    return this->m;
}

size_t CSR_Matrix::columns() const {
    return this->get_m();
}

size_t CSR_Matrix::rows() const {
    return this->get_n();
}

void CSR_Matrix::mul(CSR_Matrix &Mat) {

//    assert(m == Mat.n);

    double res;
    std::vector<double> new_A;
    std::vector<int> new_IA(this->n + 1, 0);
    std::vector<int> new_JA;

    double val;
    // for each row
    for (int i = 0; i < this->n; i++) {
        new_IA[i+1] = new_IA[i];
        // for each column
        for (int j = 0; j < Mat.m; j++) {
            res = 0.0;
            for (int k = this->IA[i]; k < this->IA[i + 1]; k++) {
                res = res + this->A[k] * Mat.get_element(this->JA[k], j);
            }

            if (res != 0) {
                new_A.push_back(res);
                new_IA[i+1] = new_IA[i+1] + 1;
                new_JA.push_back(j);
            }
        }
    }

    CSR_Matrix::set_a(new_A);
    CSR_Matrix::set_ia(new_IA);
    CSR_Matrix::set_ja(new_JA);
}

void CSR_Matrix::decompose() {
    int bn = (int) this->n / this->BLOCKSIZE_N; // number of blocks in X
    int bm = (int) this->m / this->BLOCKSIZE_M; // number of blocks in Y

    std::vector<std::vector<double>> blocks_A(bn * bm);
    std::vector<std::vector<int>> blocks_IA(bn * bm);
    std::vector<std::vector<int>> blocks_JA(bm);

    // initialize row pointer vectors
    for (auto & item : blocks_IA) {
        std::vector<int> v(this->BLOCKSIZE_N + 1, 0);
        item = v;
    }

//    for (int i = 0; i < bn; i++) {
//        std::vector<int> v(BLOCKSIZE_N + 1);
//        for (int j = 0; j < BLOCKSIZE_N + 1; j++)
//            v[j] = IA[i * BLOCKSIZE_N + j] - IA[i * BLOCKSIZE_N];
//        blocks_IA[i] = v;
//    }

    for (int i = 0; i < nnz; i++) {
        int p_elem = floor((double)JA[i] / BLOCKSIZE_M);
        blocks_JA[p_elem].push_back(JA[i] - BLOCKSIZE_M * p_elem);
    }

//    for (int i = 0; i < bn; i ++) {
//        int lbegin = IA[i*BLOCKSIZE_N];
//        int lend = IA[(i+1)*BLOCKSIZE_N];
//        for (int ii = lbegin; ii < lend; ii++) {
//            int p_elem = floor(double(JA[ii]) / BLOCKSIZE_M);
//            blocks_A[i * bn + p_elem].push_back(A[ii]);
//        }
//    }

    for (int i = 0; i < n; i++) {
        int px = floor((double) i / BLOCKSIZE_N);
        for (int j = IA[i]; j < IA[j+1]; j++) {
            int py = floor(double(JA[j]) / BLOCKSIZE_M);
            blocks_A[px * bn + py].push_back(A[j]);
            adjust_IA(blocks_IA[px * bn + py], (int) i + 1 - px * BLOCKSIZE_N);
        }
    }

    blockCSR.resize(bn * bm);
    for (int i = 0; i < bn; i++) {
        for (int j = 0; j < bm; j++) {
            blockCSR[i * bn + j].set_a(blocks_A[i * bn + j]);
            blockCSR[i * bn + j].set_ia(blocks_IA[i * bn + j]);
            blockCSR[i * bn + j].set_ja(blocks_JA[j]);
        }
    }

    // avoid too much print when miss executed
    if (this->n < 5000)
        for (auto & item : blockCSR)
            item.print(std::cout);
}

void CSR_Matrix::set_blocksize(int blocksize_n, int blocksize_m) {
    this->BLOCKSIZE_N = blocksize_n;
    this->BLOCKSIZE_M = blocksize_m;
}

void CSR_Matrix::adjust_IA(std::vector<int> &v, int pos) {
    for (int i = pos; i < v.size(); i++)
        v[i]++;
}

CSR_Matrix::~CSR_Matrix() = default;
