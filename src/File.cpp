//
// Created by willh on 29/08/2021.
//


#include "../inc/File.h"

File::File(string filename,  ios::openmode mode) {
    file.open(&filename[0], mode);

    if (!file) {
        printf("Failed to open file\n");
        exit(1);
    }
}

File::~File() {
    file.close();
//    printf("file removed");
}

std::vector<unsigned char> File::get_file_contents()
{
    // https://insanecoding.blogspot.com/2011/11/how-to-read-in-file-in-c.html

    if (file)
    {
        std::vector<unsigned char> buffer;
        file.seekg(0, std::ios::end);
        buffer.resize(file.tellg());
        file.seekg(0, std::ios::beg);
        file.read((char*)&buffer[0], buffer.size());
        return(buffer);
    }
    throw(errno);
}

CSR_Matrix File::readCSRMatrix(int file_type)
{
    // binary mode
    if (file_type == 0) {
        return read_binfile();
    }
    else if (file_type == 1) {
        return read_textfile();
    }
    else {
        exit(1);
    }
}

CSR_Matrix File::read_textfile()
{
    size_t n;

    std::string line;
    std::string val;

    vector<int> ia;
    vector<int> ja;
    vector<double> a;

    CSR_Matrix result;

//  read A
    std::getline(file, line);
    std::istringstream iss(line);
    double tmp;
    try {
        while(std::getline(iss, val, ' ')) {
            tmp = std::stof(val);
            a.push_back(tmp);
        }
    }
    catch(...) {
        cout << val << endl;
        cout << "Failed to read matrix" << endl;
        exit(1);
    }
    result.set_a(a);
    iss.clear();

    std::getline(file, line);
    iss.str(line);
    while(std::getline(iss, val, ' '))
        ia.push_back(std::stoi(val));
    result.set_ia(ia);
    iss.clear();

    std::getline(file, line);
    iss.str(line);
    while(std::getline(iss, val, ' '))
        ja.push_back(std::stoi(val));
    result.set_ja(ja);
    iss.clear();

//    printf("Matrix dimensions:\n%zu %zu\nnnz %d\n", result.get_n(), result.get_m(), result.get_nnz());
    return result;
}

CSR_Matrix File::read_binfile()
{
    vector<unsigned char> buffer = get_file_contents();

    unsigned int nnz = *(int*)(&buffer[0]);
    unsigned int n = *(int*)(&buffer[4]);
    unsigned int m = *(int*)(&buffer[8]);

//    printf("buffer size: %lu\n", buffer.size());

    CSR_Matrix result;

    vector<double> data(nnz);
    vector<int> ja(nnz);
    vector<int> ia(nnz);
    int offset;

    result.set_nnz((int)nnz);
    result.set_n(n);
    result.set_m(m);

    printf("nnz: %u\n", result.get_nnz());
    printf("n: %zu\n", result.get_n());
    printf("m: %zu\n", result.get_m());

    // read data
    offset = 12;
    for (int i = 0; i < nnz; i++)
        data[i] = *(double*)(&buffer[offset + i*8]);
    result.set_a(data);

    // read indices
    offset = offset + (int) nnz * 8;
    for (int i = 0; i < nnz; i++)
        ja[i] = *(int*)(&buffer[offset + i * 4]);
    result.set_ja(ja);

    // read pointers
    offset = offset + (int) nnz * 4;
    for (int i = 0; i < n+1; i++)
        ia[i] = *(int*)(&buffer[offset + i * 4]);
    result.set_ia(ia);

    return result;
}

