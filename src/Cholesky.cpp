//
// Created by willh on 30/08/2021.
//

#include "../inc/Cholesky.h"

Matrix<double> Cholesky::cholesky_factor(const Matrix<double>& input)
{
    assert(input.rows() == input.columns());
    size_t n = input.rows();
    Matrix<double> result(n, n);
    for (size_t i = 0; i < n; ++i) {
        for (size_t k = 0; k < i; ++k) {
            double value = input(i, k);
            for (size_t j = 0; j < k; ++j)
                value -= result(i, j) * result(k, j);
            result(i, k) = value/result(k, k);
        }
        double value = input(i, i);
        for (size_t j = 0; j < i; ++j)
            value -= result(i, j) * result(i, j);
        result(i, i) = std::sqrt(value);
    }
    return result;
}

void Cholesky::print(std::ostream& out, const Matrix<double>& a) {
    size_t rows = a.rows(), columns = a.columns();
    out << std::fixed << std::setprecision(5);
    for (size_t row = 0; row < rows; ++row) {
        for (size_t column = 0; column < columns; ++column) {
            if (column > 0)
                out << ' ';
            out << std::setw(9) << a(row, column);
        }
        out << '\n';
    }
}

CSR_Matrix Cholesky::cholesky_factor(CSR_Matrix &Mat)
{
    assert(Mat.rows() == Mat.columns());
    size_t
    n = Mat.rows();

    CSR_Matrix result;

//    for (size_t i = 0; i < n; ++i) {
//        for (size_t k = 0; k < i; ++k) {
//            double value = input(i, k);
//            for (size_t j = 0; j < k; ++j)
//                value -= result(i, j) * result(k, j);
//            result(i, k) = value/result(k, k);
//        }
//        double value = input(i, i);
//        for (size_t j = 0; j < i; ++j)
//            value -= result(i, j) * result(i, j);
//        result(i, i) = std::sqrt(value);
//    }

    return CSR_Matrix();
}
