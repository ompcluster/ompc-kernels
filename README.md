OMPC Kernels 
================================================================================

## Generate Sparse Matrix

File is saved in `data` folder.

```bash
# from root
cd scripts
python3 generate_sparse.py
```

It is possible to choose between `.txt` or `.bin` format.

In file `generate_sparse.py`:

```python
# mode for opening file
write_mode = 'wb'
# list of sizes (square nxn matrix)
runs = [10000000, 20000000]
# sparsity : nnz ~= sparsity * n ** 2
sparsity = 5e-08
```

It will also save a compressed file in `scripts/data` so we can easily load the matrix again
with python and perform manipularions, such as check our calculation, as explained below.



## Compile and run OmpCluster

Start OmpCluster Container
```bash
# from root
docker run -v $(pwd):/root/sparse --name my_ompcluster_container -it ompcluster/runtime:latest /bin/bash
docker start -i my_ompcluster_container
````

Once inside container bash, go to `/root/sparse` and execute:

```bash
mkdir -p build && cd build

# Use Clang as compiler
export CC=clang
# Generate the Makefiles
cmake -DCMAKE_BUILD_TYPE=Release ..
# Compile
make
```

To run the code (from root):
```bash
export OMP_NUM_THREADS=2
mpirun -n 2 build/linalgebra "./data/sparse20000000_5e-08p.bin"
```

## Check Calculation

From `result = A*v`, where `A` is the CSR Matrix and `v` a vector, 
it is possible to save both `result` and `v` to `out/data/`.

Currently, to save both vectors it is required to change the flag in `main.cpp`:
```c++
// from
multiply_matrix_vector(filename, 0, 0);

// to
multiply_matrix_vector(filename, 0, 1);
```

After running the program again with this change, execute `scripts/check_calculation.py`

```bash
# from root
cd scripts
python3 check_calculation.py
```

## Container - command snippets


```bash
# integration clion <-> docker container
# building container
docker build -t clion/remote-cpp-env:0.5 -f Dockerfile.remote-cpp-env .
# run container as daemon
docker run -d --cap-add sys_ptrace -p127.0.0.1:2222:22 --name clion_remote_env clion/remote-cpp-env:0.5
```