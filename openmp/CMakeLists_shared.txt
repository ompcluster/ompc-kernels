cmake_minimum_required(VERSION 3.16)
project(linalgebra)

find_package(OpenMP REQUIRED)

set(CMAKE_CXX_STANDARD 14)
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -O3 -std=c++14 -fopenmp")

add_executable(linalgebra main.cpp src/File.cpp inc/File.h src/Matrix.cpp inc/Matrix.h src/CSR_Matrix.cpp inc/CSR_Matrix.h src/SpMV.cpp inc/SpMV.h src/BCSR_Matrix.cpp inc/BCSR_Matrix.h)

target_link_libraries(linalgebra PUBLIC OpenMP::OpenMP_CXX)