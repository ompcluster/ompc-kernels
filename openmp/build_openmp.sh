#!/bin/bash
echo "************************"
echo "BUILDING SpMV openMP..."
echo "************************"

mkdir -p cmake-build-debug-wsl
cd cmake-build-debug-wsl || exit

# Use Clang as compiler

# Generate the Makefiles
cmake ..
# Compile
make

echo " "
echo "************************"
echo "...BUILD FINISHED"
echo "************************"


#COMPILER="g++"
#ALLFLAGS="-fopenmp -std=c++14"
#FILES="src/File.cpp inc/File.h src/CSR_Matrix.cpp inc/CSR_Matrix.h src/SpMV.cpp inc/SpMV.h"
#MAIN="main.cpp"
#
## Getting Started
#mkdir -p bin
#echo Building spmv as module...
#
#$COMPILER $ALLFLAGS -o ./bin/spmv $FILES $MAIN
#