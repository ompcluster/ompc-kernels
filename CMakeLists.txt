cmake_minimum_required(VERSION 3.9)
project(linalgebra)

find_package(OpenMP REQUIRED)

set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${OpenMP_C_FLAGS}")
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${OpenMP_CXX_FLAGS}")

set(CMAKE_CXX_STANDARD 14)

option(USE_OMPC "Build for OmpCluster" ON)
# Ensure we are using clang
if("${CMAKE_C_COMPILER_ID}" MATCHES "Clang")
    if(USE_OMPC)
        set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -O3 -fopenmp-targets=x86_64-pc-linux-gnu")
        set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -O3 -fopenmp-targets=x86_64-pc-linux-gnu")
    else()
        message(WARNING "OmpCluster offloading manually disable")
    endif()
else()
    message(WARNING "OmpCluster is not compatible with `${CMAKE_C_COMPILER_ID}`!")
endif()

#set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -O3 -std=c++14 -fopenmp -fopenmp-targets=x86_64-pc-linux-gnu")

add_executable(linalgebra main.cpp src/File.cpp inc/File.h src/Matrix.cpp inc/Matrix.h src/CSR_Matrix.cpp inc/CSR_Matrix.h src/SpMV.cpp inc/SpMV.h src/BCSR_Matrix.cpp inc/BCSR_Matrix.h)

target_link_libraries(linalgebra PUBLIC OpenMP::OpenMP_CXX)