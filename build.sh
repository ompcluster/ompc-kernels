#!/bin/bash
echo "************************"
echo "BUILDING SpMV OmpCluster..."
echo "************************"

mkdir -p build
cd build || exit

# Use Clang as compiler
export CC=clang
export CXX=clang++
# Generate the Makefiles
cmake -DCMAKE_BUILD_TYPE=Release ..
# Compile
make

echo " "
echo "************************"
echo "...BUILD FINISHED"
echo "************************"
