#include <time.h>
#include <iostream>
#include <vector>
#include <string>

//#include <stdexcept> // std::runtime_error
#include <sstream> // std::stringstream
#include <omp.h>
#include <cstring>

#include "inc/File.h"
#include "inc/SpMV.h"

void print_vector(vector<double> &v)
{
    for (double & i : v)
        printf("%lf,", i);
    printf("\n");
}

void save_vector(vector<double> &v, const string& filename)
{
    // Create and open a text file
    ofstream MyFile(filename);

    // Write to the file
    for (double & i : v) {
        MyFile << i << ",";
    }
    MyFile << endl;

    // Close the file
    MyFile.close();
}

void initialize_vector(vector<double> &v)
{
    for (double & i : v)
        i = (rand() % 1000) / 100.;
}

void multiply_matrixes()
{
    string file_name = string("../data/small_csr");
    File file(file_name, ios::in);

    CSR_Matrix mat = file.readCSRMatrix();

    file_name = string("../data/small_csr_2");
    File file2(file_name, ios::in);

    CSR_Matrix mat2 = file2.readCSRMatrix();

//    std::cout << "input matrix:" << std::endl;
    mat.mul(mat2);
    mat.print(std::cout);
}

void multiply_matrix_vector(string filename, int to_print, int to_save)
{
    clock_t start, end;
    double cpu_time_used;
    double t;

    start = clock();

    printf(" - Reading file\n");

    File file(std::move(filename), ios::in | ios::binary);
    CSR_Matrix mat = file.readCSRMatrix();

    end = clock();
    cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;
    printf("Time for reading file: %lf\n", cpu_time_used);

    vector<double> v(mat.columns());
    vector<double> result(mat.columns());

    initialize_vector(v);

    printf(" - Multipling\n");
    start = clock();
    t = omp_get_wtime();
    SpMV::run_serial(mat, v, result);
    t = omp_get_wtime() - t;

    end = clock();
    cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;
    printf("Time for multipling Serial: %lf\n", t);

//    SpMV::run_shared_memory(mat, v, result);
    t = omp_get_wtime();
    SpMV::run_distributed(mat, v, result);
    t = omp_get_wtime() - t;

    printf("Time for multipling Distributed: %lf\n", t);

    if (to_print == 1){
        print_vector(v);
        print_vector(result);
    }

    if (to_save == 1){
        save_vector(v, "../out/data/v.csv");
        save_vector(result, "../out/data/result.csv");
    }
}

int main(int argc, const char *argv[])
{
    printf("-----------\n");
    printf("Initializing program\n");
    printf("-----------\n");

    string input_data = string(argv[1], argv[1] + strlen(argv[1]));
    string filename = string(input_data);

    multiply_matrix_vector(filename,0, 0);

    return 0;
}
