// https://insanecoding.blogspot.com/2011/11/how-to-read-in-file-in-c.html

#include <iostream>
#include <vector>
#include <string>
#include <cstring>
#include <cstdlib>
#include <cstdio>
#include <cerrno>
#include <iostream>
#include <fstream>
#include <sstream>


using namespace std;

std::vector<unsigned char> get_file_contents(string filename)
{
  std::ifstream in(filename, std::ios::in | std::ios::binary);
  if (in)
  {
    std::vector<unsigned char> contents;
    in.seekg(0, std::ios::end);
    contents.resize(in.tellg());
    in.seekg(0, std::ios::beg);
    in.read((char*)&contents[0], contents.size());
    in.close();
    return(contents);
  }
  throw(errno);
}

int main(int argc, const char *argv[]) {

    // std::ifstream input("sparse20_0.15p.bin", std::ios::binary );

    // copies all data into buffer
    // std::vector<unsigned char> buffer(std::istreambuf_iterator<char>(input), {});
	string input_data = string(argv[1], argv[1] + strlen(argv[1]));

    vector<unsigned char> buffer = get_file_contents(input_data);

    unsigned int nnz = *(int*)(&buffer[0]);
    unsigned int n = *(int*)(&buffer[4]);
    unsigned int m = *(int*)(&buffer[8]);

    printf("nnz: %u\n", nnz);
    printf("n: %u\n", n);
    printf("m: %u\n", m);

	printf("buffer size: %lu\n", buffer.size());

    vector<double> data(nnz);
    for (int i = 0; i < nnz; i++) {
        data[i] = *(double*)(&buffer[12 + i*8]);
    }

	printf("Elem: %lf\n", data[0]);
    printf("Elem: %lf\n", data[1]);
    printf("Elem: %lf\n", data[2]);
    printf("Elem: %lf\n", data[3]);
    printf("Elem: %lf\n", data[4]);
    printf("Elem: %lf\n", data[5]);
    // for (int i = 0; i < nnz; i++) {

    // }

//    vector<char> header(12);
//    FILE *file;

//    file = fopen("sparse20_0.15p.bin", "rb");

//    if (!file) {
//        printf("Failed to open file\n");
//        exit(1);
//    }

//    while (fread(&header[0], 12, 1, file) > 0) {
//        unsigned int nnz = *(int*)(&header[0]);
//        unsigned int n = *(int*)(&header[4]);
//        unsigned int m = *(int*)(&header[8]);

//        printf("nnz: %u\n", nnz);
//        printf("n: %u\n", n);
//        printf("m: %u\n", m);
//    }


//    fclose(file);
}