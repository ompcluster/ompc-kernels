import scipy.sparse as sparse
import numpy as np
from array import array
import os

write_mode = 'wb'
# size of matrix
runs = [20000000]
sparsity = 5e-08

os.mkdir('../data')
os.mkdir('data')

for n in runs:
    mid = sparse.identity(n).tolil()

    noise_size = int(sparsity * n ** 2)
    noise = np.random.normal(0, 2, noise_size)

    noise_i = np.random.randint(0, n, size=noise_size)
    noise_j = np.random.randint(0, n, size=noise_size)

    print('noise added:')
    print(len(noise))

    for i in range(noise_size):
        mid[noise_i[i], noise_j[i]] = noise[i]

    # for value, idx, idy in zip(noise, noise_i, noise_j):
    #     # if idx != idy:
    #     mid[idx, idy] = "%.2f" % value

    mid = sparse.csr_matrix(mid)

    print('nnz:')
    print(mid.nnz)

    # check structure of sparse matrix
    # plt.figure()
    # plt.spy(mid, markersize=1)
    # plt.show()

    # Save Matrix to check later
    sparse.save_npz(f'data/sparse{n}_{sparsity}p.npz', mid)

    with open(f'../data/sparse{n}_{sparsity}p.bin', write_mode) as f:
        # write data
        if write_mode == 'wb':
            # write nnz n and m
            
            f.write((mid.nnz).to_bytes(4, byteorder='little', signed=False))
            f.seek(0, os.SEEK_END)

            f.write((mid.shape[0]).to_bytes(4, byteorder='little', signed=False))
            f.seek(0, os.SEEK_END)

            f.write((mid.shape[1]).to_bytes(4, byteorder='little', signed=False))
            f.seek(0, os.SEEK_END)

            # write A in two parts
            middle = int(mid.nnz/2)
            float_array = array('d', mid.data[:middle])
            f.write(float_array)
            f.seek(0, os.SEEK_END)

            float_array = array('d', mid.data[middle:])
            f.write(float_array)
            f.seek(0, os.SEEK_END)

            # write JA in two parts
            float_array = array('i', mid.indices[:middle])
            f.write(float_array)
            f.seek(0, os.SEEK_END)

            float_array = array('i', mid.indices[middle:])
            f.write(float_array)
            f.seek(0, os.SEEK_END)

            # write IA
            float_array = array('i', mid.indptr)
            f.write(float_array)
            f.seek(0, os.SEEK_END)

        elif write_mode == 'w':

            for entry in mid.data:
                f.write(f'{"%.2f" % entry} ')

            # new line
            f.write('\n')
            for indice in mid.indptr:
                f.write(f'{indice} ')

            # new line
            f.write('\n')
            for column in mid.indices:
                f.write(f'{column} ')
        
        print(f"saved {n}")
