#!/usr/bin/env bash

OUTPUT_FILE=times.csv

for num_threads in 1 2 4 8
do
	for run in `seq 1 10`
	do
	  echo $(cat out/spmv_${run}_${num_threads}.out | tail -n 1 | sed -n -e 's/.*multipling: //p') | tr '\n' ',' >> $OUTPUT_FILE
	done
	echo '' >> $OUTPUT_FILE
done