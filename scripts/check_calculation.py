import scipy.sparse as sparse
import numpy as np

n = 10000
filename = f'data/sparse{n}.npz'

mat = sparse.load_npz(filename)

# load vector
with open('../out/data/v.csv') as f:
    v = f.read().strip()

v = v.split(',')[:-1]
v = [float(i) for i in v]

# load result
with open('../out/data/result.csv') as f:
    result = f.read().strip()

result = result.split(',')[:-1]
result = [float(i) for i in result]

# perform operation
res = mat.dot(v)

# compare
residuo = 0.0
for r, c in zip(res, result):
    # print("%.2f" % r, "%.2f" % c)
    residuo += abs(r - c)

print('residuo:')
print(residuo)


