#!/usr/bin/env bash

#cd cmake-build-debug-wsl || exit

file="./data/sparse2000000_5e-08p.bin"

echo "******************************"
echo ""
echo "    processing file $file"
echo ""

num_process=1
num_threads=2
export OMP_NUM_THREADS=$num_threads
echo "******************************"
echo ""
echo "    running $num_process process $num_threads threads"
echo ""
time -p mpirun -n $num_process build/linalgebra $file
#./cmake-build-debug-wsl/linalgebra $file

num_process=2
num_threads=2
export OMP_NUM_THREADS=$num_threads
echo "******************************"
echo ""
echo "    running $num_process process $num_threads threads"
echo ""
time -p mpirun -n $num_process build/linalgebra $file
#./cmake-build-debug-wsl/linalgebra $file
